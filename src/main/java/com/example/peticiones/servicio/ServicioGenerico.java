package com.example.peticiones.servicio;

import java.util.Map;

public interface ServicioGenerico<T> {

    public long agregar(T t);

    public Map<Long, T> obtenerTodos();

    public T obtenerPorId(long id);

    public void reemplazarPorId(long id, T t);

    public void borrarPorId(long id);




}
