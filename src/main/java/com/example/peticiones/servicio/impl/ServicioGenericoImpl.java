package com.example.peticiones.servicio.impl;

import com.example.peticiones.servicio.ServicioGenerico;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class ServicioGenericoImpl<T> implements ServicioGenerico<T> {

    private final AtomicLong secuenciaIds = new AtomicLong(0L);
    private final Map<Long, T> hashmap = new ConcurrentHashMap<>();

    @Override
    public long agregar(T p) {
        final long id = secuenciaIds.incrementAndGet();
        this.hashmap.put(id, p);
        return id;

    }

    @Override
    public Map<Long, T> obtenerTodos() {
        return Map.copyOf(this.hashmap);
    }

    @Override
    public T obtenerPorId(long id) {
        return this.hashmap.get(id);
    }

    @Override
    public void reemplazarPorId(long id, T p) {
        if(this.hashmap.replace(id, p) == null) {
            throw new RuntimeException("No existe el T con Id. ".concat(String.valueOf(id)));
        }
    }

    @Override
    public void borrarPorId(long id) {
        final T t = this.hashmap.get(id);
        this.hashmap.remove(id, t);
    }
}
